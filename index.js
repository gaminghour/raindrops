let RainDrops=[];

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(160);
}

function draw() {
  background(255);
  updateDrops();
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}


function updateDrops(){
	if(mouseIsPressed){
		RainDrops.push(new RainDrop(mouseX,mouseY));
	}

	for (var i = RainDrops.length - 1; i >= 0; i--) {
		RainDrops[i].fall();
		RainDrops[i].show();
	}
	for (var i = RainDrops.length - 1; i >= 0; i--) {
		if(RainDrops[i].y>windowHeight){
			RainDrops.splice(i,1);
		}
	}
}

class RainDrop{
	constructor(x,y){
		this.x=x;//+random(-400,400);
		this.y=y;
		this.r = random(0,255);
		this.g = random(0,255);
		this.b = random(0,255);
		this.dir = random(-2,2);
	}

	fall= function(){
		this.y+=random(-5,3);
		this.x+=random(-3,3);
	}

	show = function(){
		noStroke();
		fill(random(0,255),random(0,255),random(0,255));
		// fill(this.r,this.g,this.b);
		ellipse(this.x,this.y,10,10);
	}
}